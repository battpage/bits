// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package gov.basis.bits.web;

import gov.basis.bits.domain.BasisBitsRole;
import gov.basis.bits.domain.BasisBitsUser;
import gov.basis.bits.service.BasisBitsRoleService;
import gov.basis.bits.service.BasisBitsUserService;
import gov.basis.bits.web.ApplicationConversionServiceFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

privileged aspect ApplicationConversionServiceFactoryBean_Roo_ConversionService {
    
    declare @type: ApplicationConversionServiceFactoryBean: @Configurable;
    
    @Autowired
    BasisBitsRoleService ApplicationConversionServiceFactoryBean.basisBitsRoleService;
    
    @Autowired
    BasisBitsUserService ApplicationConversionServiceFactoryBean.basisBitsUserService;
    
    public Converter<BasisBitsRole, String> ApplicationConversionServiceFactoryBean.getBasisBitsRoleToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<gov.basis.bits.domain.BasisBitsRole, java.lang.String>() {
            public String convert(BasisBitsRole basisBitsRole) {
                return new StringBuilder().append(basisBitsRole.getROLENAME()).toString();
            }
        };
    }
    
    public Converter<Long, BasisBitsRole> ApplicationConversionServiceFactoryBean.getIdToBasisBitsRoleConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, gov.basis.bits.domain.BasisBitsRole>() {
            public gov.basis.bits.domain.BasisBitsRole convert(java.lang.Long id) {
                return basisBitsRoleService.findBasisBitsRole(id);
            }
        };
    }
    
    public Converter<String, BasisBitsRole> ApplicationConversionServiceFactoryBean.getStringToBasisBitsRoleConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, gov.basis.bits.domain.BasisBitsRole>() {
            public gov.basis.bits.domain.BasisBitsRole convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), BasisBitsRole.class);
            }
        };
    }
    
    public Converter<BasisBitsUser, String> ApplicationConversionServiceFactoryBean.getBasisBitsUserToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<gov.basis.bits.domain.BasisBitsUser, java.lang.String>() {
            public String convert(BasisBitsUser basisBitsUser) {
                return new StringBuilder().append(basisBitsUser.getUSERNAME()).append(' ').append(basisBitsUser.getPASSWORD()).append(' ').append(basisBitsUser.getEnabled()).toString();
            }
        };
    }
    
    public Converter<Long, BasisBitsUser> ApplicationConversionServiceFactoryBean.getIdToBasisBitsUserConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, gov.basis.bits.domain.BasisBitsUser>() {
            public gov.basis.bits.domain.BasisBitsUser convert(java.lang.Long id) {
                return basisBitsUserService.findBasisBitsUser(id);
            }
        };
    }
    
    public Converter<String, BasisBitsUser> ApplicationConversionServiceFactoryBean.getStringToBasisBitsUserConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, gov.basis.bits.domain.BasisBitsUser>() {
            public gov.basis.bits.domain.BasisBitsUser convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), BasisBitsUser.class);
            }
        };
    }
    
    public void ApplicationConversionServiceFactoryBean.installLabelConverters(FormatterRegistry registry) {
        registry.addConverter(getBasisBitsRoleToStringConverter());
        registry.addConverter(getIdToBasisBitsRoleConverter());
        registry.addConverter(getStringToBasisBitsRoleConverter());
        registry.addConverter(getBasisBitsUserToStringConverter());
        registry.addConverter(getIdToBasisBitsUserConverter());
        registry.addConverter(getStringToBasisBitsUserConverter());
    }
    
    public void ApplicationConversionServiceFactoryBean.afterPropertiesSet() {
        super.afterPropertiesSet();
        installLabelConverters(getObject());
    }
    
}
