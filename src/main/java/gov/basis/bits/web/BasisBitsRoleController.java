package gov.basis.bits.web;
import gov.basis.bits.domain.BasisBitsRole;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.jquery.GvNIXWebJQuery;

@RequestMapping("/basisbitsroles")
@Controller
@RooWebScaffold(path = "basisbitsroles", formBackingObject = BasisBitsRole.class)
@GvNIXWebJQuery
public class BasisBitsRoleController {
}
