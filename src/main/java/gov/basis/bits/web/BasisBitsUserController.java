package gov.basis.bits.web;
import gov.basis.bits.domain.BasisBitsUser;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.jquery.GvNIXWebJQuery;

@RequestMapping("/basisbitsusers")
@Controller
@RooWebScaffold(path = "basisbitsusers", formBackingObject = BasisBitsUser.class)
@GvNIXWebJQuery
public class BasisBitsUserController {
}
