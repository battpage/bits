// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package gov.basis.bits.web;

import gov.basis.bits.domain.BasisBitsRole;
import gov.basis.bits.service.BasisBitsRoleService;
import gov.basis.bits.service.BasisBitsUserService;
import gov.basis.bits.web.BasisBitsRoleController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect BasisBitsRoleController_Roo_Controller {
    
    @Autowired
    BasisBitsRoleService BasisBitsRoleController.basisBitsRoleService;
    
    @Autowired
    BasisBitsUserService BasisBitsRoleController.basisBitsUserService;
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String BasisBitsRoleController.create(@Valid BasisBitsRole basisBitsRole, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, basisBitsRole);
            return "basisbitsroles/create";
        }
        uiModel.asMap().clear();
        basisBitsRoleService.saveBasisBitsRole(basisBitsRole);
        return "redirect:/basisbitsroles/" + encodeUrlPathSegment(basisBitsRole.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String BasisBitsRoleController.createForm(Model uiModel) {
        populateEditForm(uiModel, new BasisBitsRole());
        return "basisbitsroles/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String BasisBitsRoleController.show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("basisbitsrole", basisBitsRoleService.findBasisBitsRole(id));
        uiModel.addAttribute("itemId", id);
        return "basisbitsroles/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String BasisBitsRoleController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("basisbitsroles", basisBitsRoleService.findBasisBitsRoleEntries(firstResult, sizeNo));
            float nrOfPages = (float) basisBitsRoleService.countAllBasisBitsRoles() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("basisbitsroles", basisBitsRoleService.findAllBasisBitsRoles());
        }
        return "basisbitsroles/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String BasisBitsRoleController.update(@Valid BasisBitsRole basisBitsRole, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, basisBitsRole);
            return "basisbitsroles/update";
        }
        uiModel.asMap().clear();
        basisBitsRoleService.updateBasisBitsRole(basisBitsRole);
        return "redirect:/basisbitsroles/" + encodeUrlPathSegment(basisBitsRole.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String BasisBitsRoleController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, basisBitsRoleService.findBasisBitsRole(id));
        return "basisbitsroles/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String BasisBitsRoleController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        BasisBitsRole basisBitsRole = basisBitsRoleService.findBasisBitsRole(id);
        basisBitsRoleService.deleteBasisBitsRole(basisBitsRole);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/basisbitsroles";
    }
    
    void BasisBitsRoleController.populateEditForm(Model uiModel, BasisBitsRole basisBitsRole) {
        uiModel.addAttribute("basisBitsRole", basisBitsRole);
        uiModel.addAttribute("basisbitsusers", basisBitsUserService.findAllBasisBitsUsers());
    }
    
    String BasisBitsRoleController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
