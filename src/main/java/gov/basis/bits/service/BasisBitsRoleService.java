package gov.basis.bits.service;
import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { gov.basis.bits.domain.BasisBitsRole.class })
public interface BasisBitsRoleService {
}
