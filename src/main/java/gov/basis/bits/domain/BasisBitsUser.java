package gov.basis.bits.domain;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class BasisBitsUser {
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "users")  
	private Set<BasisBitsRole> groups = new HashSet<BasisBitsRole>();

    /**
     */
    @NotNull
    @Size(max = 25)
    private String USERNAME;

    /**
     */
    @NotNull
    @Size(max = 25)
    private String PASSWORD;

    /**
     */
    @NotNull
    @Size(max = 1)
    private String enabled;
}
