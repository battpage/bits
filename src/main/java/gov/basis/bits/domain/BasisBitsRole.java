package gov.basis.bits.domain;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class BasisBitsRole {
	
	@ManyToMany(cascade = CascadeType.ALL)  
	private Set<BasisBitsUser> users = new HashSet<BasisBitsUser>();  


    /**
     */
    @NotNull
    @Size(max = 12)
    private String ROLENAME;
}
